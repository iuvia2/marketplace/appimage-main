#!/usr/bin/env python3

import os, sys

import click

# If these directories do not exist, we look them up from the AppImage path by creating {name}.data
APPIMAGE_LOCATION = os.environ.setdefault(
    "APPIMAGE", os.path.dirname(os.path.abspath(sys.argv[0]))
)
APPIMAGE_DATA_LOCATION = APPIMAGE_LOCATION + ".data" + os.path.sep

STATE_DIRECTORY = os.environ.setdefault(
    "STATE_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "lib")
)
RUNTIME_DIRECTORY = os.environ.setdefault(
    "RUNTIME_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "run")
)
CACHE_DIRECTORY = os.environ.setdefault(
    "CACHE_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "cache")
)
LOGS_DIRECTORY = os.environ.setdefault(
    "LOGS_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "log")
)
CONFIGURATION_DIRECTORY = os.environ.setdefault(
    "CONFIGURATION_DIRECTORY", os.path.join(APPIMAGE_DATA_LOCATION + "etc")
)


@click.group()
@click.option("--iuvia-platform", is_flag=True)
def cli(iuvia_platform):
    print("PLATFORM", iuvia_platform)
    if not iuvia_platform:
        raise Exception("Unknown usage")


@cli.command("metadata")
@click.option("--icon", "-i", nargs=1)
def metadata(icon=None):
    if not icon:
        print("Return PB")
    else:
        print("LOOKUP ", icon)


@cli.command("install")
def platform_install():
    click.echo("Nothing to do")


@cli.command("uninstall")
def platform_uninstall():
    click.echo(
        "Nothing to do. Remaining data is in {APPDATA}".format(
            APPDATA=STATE_DIRECTORY,
        )
    )


@cli.command("run")
def platform_run():
    print("Running..")


if __name__ == "__main__":
    cli()
