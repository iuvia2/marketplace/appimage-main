#!/bin/bash

HERE="$(dirname "$(readlink -f "${0}")")"
METADATA_FILENAME="metadata"

usage() {
  echo 
  echo "Usage: $0 --metadata"
  echo "Usage: $0 --metadata -i <icon_filename>"
  echo "Usage: $0 --metadata -h"
  echo 
  echo 'OPTIONS'
  echo '    --metadata                 Display the protobuf metadata for this AppImage'
  echo '    --metadata -i <name>       Return binary of the icon'
  echo '    --metadata -h              Display this help'
  exit 1
}

metadata() {
  if [ ! -f "$HERE/${METADATA_FILENAME}.bin" ]; then
    if [ ! -f "$HERE/${METADATA_FILENAME}.txt" ]; then
      exit 1
    else
      protoc --encode=io.iuvia.AppImage appimage.proto < ${METADATA_FILENAME}.txt > ${METADATA_FILENAME}.bin
    fi
  fi
  cat "$HERE/${METADATA_FILENAME}.bin"
  exit 0
}

icon() {
  if [ ! -f "$HERE/${ICON_FILENAME}" ]; then
    exit 1
  else
    cat "$HERE/$ICON_FILENAME"
    exit 0
  fi
}

metadataoptions() {
  if [ -z $1 ]; then
    metadata
  else
    case "$1" in
      -i)
        if [ -z $2 ]; then
          usage
        else
          ICON_FILENAME=$2
          icon
        fi
        ;;
      -h)
        usage
        ;;
      *)
        usage
        ;;
    esac
  fi
}


if [[ $1 = "--iuvia-platform" ]]; then
	shift
	if [[ $1 = "--debug-shell" ]]; then
		bash
	fi
	if [[ $1 = "--metadata" ]]; then
		shift
		metadataoptions "$@"
	fi
fi
