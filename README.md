# AppImage directory and generation tools

The content of this repository eases AppImage creation.

It provides a Docker container to download appimagetool and create easily AppImages from the source directory.

## Build the appimagetool image
```
docker build -t appimagetool .
```
## Run the appimagetool container

Given an AppImage source directory (in this example, `HelloWorldApp`) inside the `/appimages` dir, run the
following command. The runnable .AppImage file will be stored in the `/appimages` directory.

```
docker run -v `pwd`/appimages:/appimages --privileged --rm -it appimagetool ./appimagetool.sh HelloWorldApp
```

## Build AppImage using appimagetool

You can also download appimagetool from https://github.com/AppImage/AppImageKit and run the following command:

```
ARCH=x86_64 ./appimagetool-x86_64.AppImage HelloWorldApp/ ./HelloWorldApp.AppImage
```

## Generate AppImage sources

An example can be seen in `./appimages/HelloWorldApp`. The important files are:

* `AppRun`: Runnable script with the implementation of --metadata to read the ProtoBuf binary representation
* `metadata.txt`: Text representation of the AppImage metadata
* `metadata.bin`: Binary representation of the AppImage metadata. If this binary is not generated, AppRun will
generate it from the `metadata.txt` file.

TODO: This is an example, the HelloWorldApp AppImage only prints metadata in the correct format so that we
can test how this communicates with django

## To encode a protobuf text message into binary
```
protoc --encode=io.iuvia.AppImage appimage.proto < metadata.txt > metadata.bin
```
